'''
Created on Jun 25, 2018

@author: Alex
'''
#python3 directory
#d:\users\alex\appdata\local\programs\python\python36\lib\site-packages

import sc2
import random
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from CustomBaseBot import BaseBot
from sc2.constants import *

class TestBot(BaseBot):
    
    def __init__(self):
        self.ITERATION_PER_MINUTE = 165
        self.boolswitch = False
          
        
    async def on_step(self, iteration):
        self.iteration = iteration
        hatches = self.units(HATCHERY) | self.units(LAIR) | self.units(HIVE)

        await self.tell_orders()

        await self.execute_order_queue()

    # async def spread_creep(self, hatches):
    #     queens = self.units(QUEEN)
    #     tumors = self.units(CREEPTUMORBURROWED) #check for ability to see if can spread or not
    #     for t in tumors:
    #         if BUILD_CREEPTUMOR_TUMOR in await self.get_available_abilities(t):  # always spread with active tumors
    #             p = await self.find_tumor_placement(t.position,self.game_info.map_center, max_distance = 8)  # find tumor placement
    #             await self.do(t(BUILD_CREEPTUMOR_TUMOR, p))
    #     for q in queens.idle:
    #         if BUILD_CREEPTUMOR_QUEEN in await self.get_available_abilities(q):
    #             p = await self.find_tumor_placement(q.position, self.game_info.map_center, max_distance = 50) #find tumor placement
    #             await self.do(q(BUILD_CREEPTUMOR_QUEEN, p))

    async def tell_orders(self):
        print(self.known_enemy_units)