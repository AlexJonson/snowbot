import sc2, sys
from __init__ import run_ladder_game
from sc2 import Race, Difficulty
from sc2.player import Bot, Computer

# Load bot
from testBot import TestBot

bot = Bot(Race.Zerg, TestBot())
computer = Computer(Race.Protoss, Difficulty.Easy)
# Start game
if __name__ == '__main__':
    if "--LadderServer" in sys.argv:
        # Ladder game started by LadderManager
        print("Starting ladder game...")
        run_ladder_game(bot)
    else:
        # Local game
        print("Starting local game...")
        sc2.run_game(sc2.maps.get("CatalystLE"), [
            bot,
            computer
        ], realtime=False)
        #has trouble with terran tanks/ bio  --> a moves to the nice choke
        
        
        # possible maps to test on:
        # 4 player: (4)DarknessSanctuaryLE , 
        # 2 player: (2)LostandFoundLE, (2)DreamcatcherLE, (2)AcidPlantLE, (2)16-BitLE, CatalystLE
        # special: (2)RedshiftLE