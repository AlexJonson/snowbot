'''
Created on Jun 25, 2018

@author: Alex Jonson

'''
#python3 directory
#d:\users\alex\appdata\local\programs\python\python36\lib\site-packages

#https://github.com/Dentosal/python-sc2 --- python-sc2 Github


import sc2
import random
from CustomBaseBot import BaseBot
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *

class SpaceBot(BaseBot):
    
    def __init__(self):
        self.SECONDS_PER_MINUTE = 60
        self.gameTime = 0
        self.GuessedDroneCountChange = 0 #all "stats" only update at the start of the step  they dont proactivly update --> these keep track of changes as it goes on --> makes bot smarter so it wont over make stuff
        self.GuessedArmySupplyChange = 0
        self.GuessedMineralsSpent = 0
        self.GuessedGasSpent = 0

        self.currentVersion = '3.1.0' #update when submitted to ladder: last submitted 9/19/2018



        self.enemyRace = ""
        self.setRace = False

        #saved locations
        self.expansions = []

        #known unit IDs
        self.currentScoutID = 0
        self.creepSpreadQueens = []
        self.mainHatch = 0

        #Do once flags
        self.firstExpand = True
        self.sendMessage = True
        self.firstScoutHappened = False
        self.movedFirstOverlord = False
        self.setFirstHatch = False

        #attack/retreat/defend flags
        self.goHard = False
        self.defendTheCreep = False

        #build flags
        self.proxyFirstHatch = False  # controls if first hatch is proxied

        #anti-cheese flags

        #anti-build flags
        self.enemyIsGoingForAir = False

    async def on_step(self, iteration):
        self.set_enemy_race()
        self.record_main_hatchery()
        self.gameTime = self.get_game_time()
        hatches = self.units(HATCHERY) | self.units(LAIR) | self.units(HIVE)
        droneCountChange = 0
        await self.distribute_workers()
        await self.roachMicro()
        await self.lurkerMicro()
        await self.build_workers(hatches, droneCountChange)
        await self.build_overlords(hatches)
        await self.build_spawningpool(hatches)
        await self.build_lair()
        await self.manageLarva(hatches)
        await self.expandWell()
        await self.buildgas(hatches)
        await self.buildqueens(hatches)
        await self.larvainject(hatches)
        await self.controlArmy(hatches)
        await self.getUpgrades(hatches)
        await self.transfuse()
        await self.build_roachwarren(hatches)
        await self.build_evolution(hatches)
        await self.morphOverseer()
        await self.build_hydraden(hatches)
        await self.build_lurkerden(hatches)
        await self.build_infestationpit(hatches)
        await self.build_hive()
        await self.initialScout()
        await self.moveFirstOverlord()
        await self.setup_defense(hatches) #currently causes big delay in 4th hatch
        await self.build_spire(hatches)
        await self.build_greateSpire()
        await self.morphBroodlord()
        await self.morphLurker()
        await self.enemyGoingAir()
        await self.tell_version()
        await self.spread_creep(hatches)
        await self.cancel_buildings()        
        #must go at the end!!!
        await self.execute_order_queue() #https://github.com/Hannessa/sc2-bots/blob/master/cannon-lover/base_bot.py -- how to use order Queue because performing 1 do at the end is more efficient 


    async def tell_version(self):
            if(self.sendMessage):
                if(self.proxyFirstHatch):
                    self.sendMessage = False
                    message = str('SpaceBot: ' + self.currentVersion)
                    await self.chat_send(message) #busted on desktop for some reason
                else:
                    self.sendMessage = False
                    message = str('SnowBot: ' + self.currentVersion)
                    await self.chat_send(message)
    '''
    returns list
    1st player is you
    2nd player is opponent
    
    Terran = 1
    Zerg = 2
    Protoss = 3
    '''
    def set_enemy_race(self):
        if not self.setRace:
            self.enemyRace = self.game_info.player_races[2]
            self.setRace = True
            if self.enemyRace == 1:
                print("Opponent is Terran")
            elif self.enemyRace == 2:
                print("Opponent is Zerg")
            elif self.enemyRace == 3:
                print("Opponent is Protoss")
            elif self.enemyRace == 4:
                print("Opponent is Random, trying to find out actual race")
        if self.setRace and self.enemyRace == 4:
            enemyStuff = self.known_enemy_units
            if enemyStuff(SCV).amount > 0 or enemyStuff(SUPPLYDEPOT).amount > 0 or enemyStuff(BARRACKS).amount > 0 or enemyStuff(COMMANDCENTER).amount > 0:
                self.enemyRace = 1
                print("Opponent is Terran")
            elif enemyStuff(DRONE).amount > 0 or enemyStuff(OVERLORD).amount > 0 or enemyStuff(HATCHERY).amount > 0:
                self.enemyRace = 2
                print("Opponent is Zerg")
            elif enemyStuff(PROBE).amount > 0 or enemyStuff(PYLON).amount > 0 or enemyStuff(GATEWAY).amount > 0 or enemyStuff(NEXUS).amount > 0:
                self.enemyRace = 3
                print("Opponent is Protoss")
                
    async def build_workers(self, hatches, droneCountChange):
        if not self.needArmyNow(hatches): #potentially add make wait for important stuff to happen
            for larva in self.units(LARVA).ready:
                if(self.can_afford(DRONE) and (self.supply_left > 1 or self.already_pending(OVERLORD)) and self.supply_left > 0 and hatches.ready.amount * 20 >= len(self.units(DRONE)) and (self.units(DRONE).amount + droneCountChange + self.already_pending(DRONE)) < 75): #75 worked well
                    droneCountChange = droneCountChange + 1
                    await self.do(larva.build(DRONE))
    
    def needArmyNow(self, hatches): #pause spending on everything except army
        #if need army dont waste larva on drones / money on upgrades or buildings
        for loc in self.enemy_start_locations:
            proxyHatch = hatches.closer_than(20, loc)
            if proxyHatch.amount > 0 and self.known_enemy_units.amount > 0:
                if self.known_enemy_units.closer_than(20, proxyHatch[0].position).amount > 0:
                    return True

        if(len(self.known_enemy_units.not_structure) > 1):
            myArmyValue = self.computeArmyValue()
            enemyArmy = self.known_enemy_units.not_structure
            if enemyArmy.amount >= myArmyValue - 10:
                return True

        return False
    
    def computeArmyValue(self): #returns army supply used (not by drones)
        return self.units(ROACH).ready.amount + self.units(HYDRALISK).ready.amount + self.units(BROODLORD).ready.amount + (self.units(ZERGLING).ready.amount / 2)

    async def buildqueens(self, hatches):  # currently only builds 1 queen per hatch
        for hatch in hatches.ready.noqueue:
            if self.can_afford(QUEEN) and self.supply_left > 1 and len(self.units(QUEEN)) < (hatches.amount) and len(self.units(SPAWNINGPOOL).ready) > 0 and hatch.noqueue and self.units(QUEEN).amount < 5:
                await self.do(hatch.train(QUEEN))
                break

    #spread creep is slow once many tumors are on the map
    async def spread_creep(self, hatches):
        allQueens = self.units(QUEEN).ready
        creepQueens = self.units(QUEEN).keep_tags(self.creepSpreadQueens)
        injectQueens = self.units(QUEEN).remove_tags(self.creepSpreadQueens)
        # look for more queens to be added to creep spread duty
        if(injectQueens.amount > hatches.ready.amount and injectQueens.amount > 0):
            self.creepSpreadQueens.append(injectQueens[0].tag)#add a queen to creep spread
        elif(injectQueens.amount > 5):
            self.creepSpreadQueens.append(injectQueens[0].tag)
        elif(injectQueens.amount >= 2 and creepQueens.amount == 0):
            self.creepSpreadQueens.append(injectQueens[0].tag)
        #

        tumors = self.units(CREEPTUMORBURROWED) #check for ability to see if can spread or not
        for t in tumors:
            if BUILD_CREEPTUMOR_TUMOR in await self.get_available_abilities(t):  # always spread with active tumors
                p = await self.find_tumor_placement(near=t.position, place_towards=self.game_info.map_center, max_distance = 8)  # find tumor placement
                await self.do(t(BUILD_CREEPTUMOR_TUMOR, p))
        for q in creepQueens.doesnt_have_orders([BUILD_CREEPTUMOR, ATTACK, TRANSFUSION_TRANSFUSION]):
            if BUILD_CREEPTUMOR_QUEEN in await self.get_available_abilities(q):
                p = await self.find_tumor_placement(near=q.position, place_towards=self.game_info.map_center, max_distance = 20) #find tumor placement
                await self.do(q(BUILD_CREEPTUMOR_QUEEN, p))

    def record_main_hatchery(self):
        if not self.setFirstHatch:
            self.mainHatch = self.units(HATCHERY)[0].tag
            self.setFirstHatch = True

    # needs update --> usually gets chased down and killed by enemy workers
    async def initialScout(self):
        if not self.firstScoutHappened:
            self.firstScoutHappened = True
            positionsToScout = self.enemy_start_locations
            scout = self.units(DRONE).ready[0]
            self.currentScoutID = scout.tag
            for s in positionsToScout:
                await self.do(scout.move(s, queue=True))
        elif self.units.find_by_tag(self.currentScoutID) is not None and not self.can_afford(HATCHERY) and not self.already_pending(HATCHERY):
            scout = self.units.find_by_tag(self.currentScoutID)
            if self.known_enemy_units.not_structure.amount > 0:
                if self.known_enemy_units.not_structure.closest_to(scout.position).distance_to(scout.position) < 5:
                    enemy = self.known_enemy_units.not_structure.closest_to(scout.position)
                    moveHere = scout.position.to2.towards(enemy.position, distance=-3)
                    await self.do(scout.move(moveHere))
                elif self.known_enemy_structures.amount > 0:
                    if self.known_enemy_structures.closest_to(scout.position).distance_to(scout.position) > 6:
                        await self.do(scout.move(self.known_enemy_structures.closest_to(scout.position).position))
                    else:
                        await self.do(scout.hold_position())
                else:
                    for s in self.enemy_start_locations:
                        await self.do(scout.move(s))
            elif self.known_enemy_structures.amount > 0:
                if self.known_enemy_structures.closest_to(scout.position).distance_to(scout.position) > 6:
                    await self.do(scout.move(self.known_enemy_structures.closest_to(scout.position).position))
                else:
                    await self.do(scout.hold_position())
            else:
                for s in self.enemy_start_locations:
                    await self.do(scout.move(s))

    async def moveFirstOverlord(self): #moves to center currently
        if self.movedFirstOverlord is False:
            self.movedFirstOverlord = True
            overlord = self.units(OVERLORD).ready[0]
            await self.do(overlord.move(self.game_info.map_center))
                
    async def setup_defense(self, hatches): #builds spores and spines near hatches
        if (self.gameTime > self.SECONDS_PER_MINUTE * 5 and hatches.amount > 3 or self.gameTime > self.SECONDS_PER_MINUTE * 7) and await self.hasLair(): #after 5 mins build defense #waits till 4th is down
            spines = self.units(SPINECRAWLER)
            spores = self.units(SPORECRAWLER)
            for h in hatches.ready:
                if self.can_afford(SPINECRAWLER) and self.units(SPAWNINGPOOL).ready.amount > 0:
                    if len(spines) is 0:
                        await self.build(SPINECRAWLER, near=h.position)
                    elif spines.closest_to(h.position).distance_to(h.position) > 10 and not self.already_pending(SPINECRAWLER) > 1: # dont build more than x amount  fail safe bec wont deal with drones pending correctly
                        await self.build(SPINECRAWLER, near = h.position)
                if self.can_afford(SPORECRAWLER) and self.units(SPAWNINGPOOL).ready.amount > 0:
                    if len(spores) is 0:
                        await self.build(SPORECRAWLER, near=h.position)
                    elif spores.closest_to(h.position).distance_to(h.position) > 10 and not self.already_pending(SPORECRAWLER) > 1:
                        await self.build(SPORECRAWLER, near=h.position)


    def find_best_near_for_build(self):
        #pick one hatchery to place things near
        main_hatch = self.units.find_by_tag(self.mainHatch)
        if main_hatch is None:
            lair = self.units(LAIR)
            if not lair.exists:
                hive = self.units(HIVE)
                if not hive.exists:
                    hatch = self.units(HATCHERY) #assume hatchery exists for now
                    self.mainHatch = hatch.first.tag
                    main_hatch = self.units.find_by_tag(self.mainHatch)
                else:
                    self.mainHatch = hive.first.tag
                    main_hatch = self.units.find_by_tag(self.mainHatch)
            else:
                self.mainHatch = lair.first.tag
                main_hatch = self.units.find_by_tag(self.mainHatch)
        return main_hatch
            
    async def build_overlords(self, hatches): # makes a lot of overlords at once if has money
        for larva in self.units(LARVA).ready:
            if self.can_afford(OVERLORD) and self.supply_left < 5 * len(hatches) and self.already_pending(OVERLORD) < len(hatches.ready) and self.supply_cap < 200:
                await self.do(larva.train(OVERLORD))

    async def build_spawningpool(self, hatches):
        if self.can_afford(SPAWNINGPOOL) and self.supply_used > 14 and not self.already_pending(SPAWNINGPOOL) and len(self.units(SPAWNINGPOOL)) < 1:
            if hatches.exists:
                await self.build(SPAWNINGPOOL, near=self.find_best_near_for_build(), placement_step=4)
                
    async def build_roachwarren(self, hatches):
        if self.can_afford(ROACHWARREN) and self.units(SPAWNINGPOOL).ready.amount > 0 and self.units(DRONE).amount > 30 and not self.units(ROACHWARREN).amount > 0 and not self.already_pending(ROACHWARREN):
            if hatches.exists:
                await self.build(ROACHWARREN, near=self.find_best_near_for_build(), placement_step=4)
                
    async def build_evolution(self, hatches):
        if self.can_afford(EVOLUTIONCHAMBER) and self.units(EVOLUTIONCHAMBER).amount + self.already_pending(EVOLUTIONCHAMBER) < 3 and self.gameTime > 6 * self.SECONDS_PER_MINUTE and hatches.amount > 2:
            await self.build(EVOLUTIONCHAMBER, near=self.find_best_near_for_build(), placement_step=4)
    
    async def build_spire(self, hatches):
        if self.can_afford(SPIRE) and self.units(SPIRE).amount < 1 and self.already_pending(SPIRE) < 1 and self.units(INFESTATIONPIT).amount > 0 and self.units(GREATERSPIRE).amount < 1:
            await self.build(SPIRE, near=self.find_best_near_for_build(), placement_step=4)
    
    async def build_greateSpire(self):
        if self.can_afford(GREATERSPIRE) and self.units(SPIRE).ready.noqueue.amount > 0 and self.already_pending(GREATERSPIRE) < 1 and self.units(GREATERSPIRE).amount < 1 and self.units(HIVE).ready.amount > 0:
            await self.do(self.units(SPIRE).ready[0](UPGRADETOGREATERSPIRE_GREATERSPIRE))
    
    async def build_hydraden(self, hatches): #builds a hydra den after 6 min once has money
        if self.can_afford(HYDRALISKDEN) and self.units(HYDRALISKDEN).amount < 1 and not self.already_pending(HYDRALISKDEN) and hatches(LAIR).ready.amount > 0 and self.gameTime > self.SECONDS_PER_MINUTE * 5:
            await self.build(HYDRALISKDEN, near=self.find_best_near_for_build(), placement_step=4)
            
    async def build_infestationpit(self, hatches):
        if self.can_afford(INFESTATIONPIT) and self.units(INFESTATIONPIT).amount < 1 and not self.already_pending(INFESTATIONPIT) and hatches(LAIR).ready.amount > 0 and self.units(HYDRALISKDEN).ready.amount > 0:
            await self.build(INFESTATIONPIT, near=self.find_best_near_for_build(), placement_step=4)
            
    async def build_lair(self): #builds lairs until 1 is finished
        if self.can_afford(LAIR) and self.units(DRONE).amount > 24 and not await self.hasLair() and self.units(HIVE).amount < 1 and self.units(HATCHERY).ready.noqueue.amount > 0:
            await self.do(self.units(HATCHERY).ready.noqueue[0](UPGRADETOLAIR_LAIR))    
            
    async def build_hive(self):
        if self.can_afford(HIVE) and self.units(LAIR).ready.noqueue.amount == self.units(LAIR).ready.amount and self.units(DRONE).amount > 50 and not self.units(HIVE).amount > 0 and self.units(LAIR).amount > 0:
            await self.do(self.units(LAIR).ready.noqueue[0](UPGRADETOHIVE_HIVE))
            
    async def build_lurkerden(self, hatches):
        if self.can_afford(LURKERDENMP) and self.units(LAIR).amount + self.units(HIVE).amount > 0 and self.units(HYDRALISKDEN).ready.amount > 0 and self.units(LURKERDENMP).amount < 1 and not self.already_pending(LURKERDENMP):
            await self.build(LURKERDENMP, near=self.find_best_near_for_build(), placement_step=4)
    # this will turn into build army and maybe even manage larva
                 
    async def manageLarva(self, hatches):
        if not self.supply_used == 200 and await self.doImportantStuffFirst(hatches):
            for larva in self.units(LARVA).ready:
                if(len(self.units(SPAWNINGPOOL).ready)) >= 1 and self.supply_left > 0 and self.can_afford(ZERGLING) and (self.units(ZERGLING).amount * 4 < (self.units(ROACH).amount + self.units(HYDRALISK).amount) and self.vespene < 200 or (self.units(ROACHWARREN).ready.amount < 1 and self.units(HYDRALISKDEN).ready.amount < 1)):
                    await self.do(larva.build(ZERGLING))
                elif self.units(ROACHWARREN).ready.amount >= 1 and self.supply_left > 1 and self.can_afford(ROACH) and (self.units(ROACH).amount < self.units(HYDRALISK).amount or self.units(HYDRALISKDEN).ready.amount < 1) and not self.enemyIsGoingForAir and not self.units(GREATERSPIRE).ready.amount > 0:
                    await self.do(larva.build(ROACH))
                elif (self.units(HYDRALISKDEN).ready.amount >= 1 and self.supply_left > 1 and self.can_afford(HYDRALISK) and self.units(GREATERSPIRE).ready.amount < 1) or (self.units(GREATERSPIRE).ready.amount > 0 and self.units(HYDRALISKDEN).ready.amount >= 1 and (self.supply_left >= 30 or self.units(CORRUPTOR).amount >= 8) and self.can_afford(HYDRALISK)) or (self.enemyIsGoingForAir and self.units(HYDRALISKDEN).ready.amount > 0 and self.supply_left > 1 and self.can_afford(HYDRALISK)):
                    await self.do(larva.build(HYDRALISK))
                elif (self.units(SPIRE).ready.amount >= 1 or self.units(GREATERSPIRE).ready.amount >=1) and self.supply_left > 1 and self.can_afford(CORRUPTOR) and self.units(CORRUPTOR).amount < 8:
                    await self.do(larva.build(CORRUPTOR))
    
    async def hasLair(self):
        if(self.units(LAIR).amount > 0):
            return True
        morphingYet = False
        for h in self.units(HATCHERY):
            if CANCEL_MORPHLAIR in await self.get_available_abilities(h):
                morphingYet = True
                break
        if morphingYet:
            return True   
        return False
           
    async def doImportantStuffFirst(self, hatches): #pause spending on army if returns false # changed from def to async def to handle checking for lair being built --> not sure if correct
        if self.needArmyNow(hatches):
            return True 
        elif self.gameTime > self.SECONDS_PER_MINUTE * 4 and not await self.hasLair() and self.units(HIVE).amount < 1  and self.units(HATCHERY).amount > 1: #make sure gets lair
            return False
        elif self.gameTime > (self.SECONDS_PER_MINUTE * 3) and hatches.amount < 3 and not self.gameTime > self.SECONDS_PER_MINUTE * 4: #make sure gets 3 expo correctly
            return False
        elif self.units(LAIR).ready.amount > 0 and self.units(OVERSEER).amount < 2: # make sure 2 overseer with army at all times
            return False
        elif self.units(LAIR).ready.amount > 0 and self.units(HYDRALISKDEN).amount  < 1 and self.gameTime > self.SECONDS_PER_MINUTE * 6: #makes sure gets hydra den
            return False
        elif self.units(HYDRALISKDEN).ready.amount > 0 and self.units(INFESTATIONPIT).amount < 1 and self.gameTime > self.SECONDS_PER_MINUTE * 8: #make sure gets infestation pit
            return False
        elif self.units(INFESTATIONPIT).ready.amount > 0 and self.units(HIVE).amount < 1 and self.units(LAIR).ready.noqueue.amount == self.units(LAIR).ready.amount and self.gameTime > self.SECONDS_PER_MINUTE * 9: #makes sure gets hive
            return False
        elif self.units(GREATERSPIRE).ready.amount > 0 and self.units(CORRUPTOR).ready.amount > 0: #make broodlords
            return False
        elif self.units(LURKERDENMP).ready.amount > 0 and self.units(HYDRALISK).amount / 4 > (self.units(LURKERMP).amount + self.units(LURKERMPEGG).amount) and not self.enemyIsGoingForAir:
            return False
        return True
    
    async def expandWell(self): # make expand slower in late game
        if self.proxyFirstHatch:
            if self.firstExpand and self.units(HATCHERY).amount > 0:
                if self.known_enemy_structures.amount > 0:
                    if self.units(HATCHERY).closest_to(self.known_enemy_structures[0]).distance_to(self.known_enemy_structures[0]) < 15:
                        self.firstExpand = False
            if(self.firstExpand and self.can_afford(HATCHERY) and not self.already_pending(HATCHERY)):
                if len(self.enemy_start_locations) > 1:
                    if self.known_enemy_structures.amount > 0:
                        await self.build(HATCHERY, near=self.known_enemy_structures[0], placement_step=2, unit = self.units(DRONE).closest_to(self.known_enemy_structures[0]))
                else:
                    await self.build(HATCHERY, near=self.enemy_start_locations[0], unit = self.units(DRONE).closest_to(self.enemy_start_locations[0]), placement_step=2)
            elif (self.can_afford(HATCHERY) and len(self.units(DRONE)) > (len(self.units(HATCHERY)) + self.already_pending(HATCHERY)) * 18) or (self.can_afford(HATCHERY) and not self.already_pending(HATCHERY) > 1):
                await self.expand_now()
        else:
            if (self.can_afford(HATCHERY) and len(self.units(DRONE)) > (len(self.units(HATCHERY)) + self.already_pending(HATCHERY)) * 18) or (self.can_afford(HATCHERY) and not self.already_pending(HATCHERY) > 1):
                await self.expand_now()

    #take gases slower --> usually takes 3 by 3 min --> make expand faster ?        
    async def buildgas(self, hatches):
        for hatch in hatches:
            vespene = self.state.vespene_geyser.closer_than(10, hatch)
            for v in vespene:
                if (self.can_afford(EXTRACTOR) and (len(self.units(DRONE)) > (len(self.units(EXTRACTOR)) * 16) + 20 or (len(self.units(DRONE)) > 16 and self.units(EXTRACTOR).amount < 1)) and not self.already_pending(EXTRACTOR)) or (self.can_afford(EXTRACTOR) and self.gameTime > self.SECONDS_PER_MINUTE * 9):
                    worker = self.select_build_worker(v.position)
                    if not self.units(EXTRACTOR).closer_than(1, v).exists and not self.already_pending(EXTRACTOR) and not worker is None:
                        await self.do(worker.build(EXTRACTOR, v))
                        break
    

    async def larvainject(self, hatches):
        toBreak = False
        for hatch in hatches(HATCHERY).ready | hatches(LAIR) | hatches(HIVE):
            for queen in self.units(QUEEN).remove_tags(self.creepSpreadQueens).doesnt_have_order(EFFECT_INJECTLARVA).ready.prefer_close_to(hatch.position):
                if not hatch.has_buff(QUEENSPAWNLARVATIMER) and queen.energy >= 25: #QUEENSPAWNLARVATIMER
                    await self.do(queen(EFFECT_INJECTLARVA, target=hatch))
                    break
            if(toBreak):
                break
            #find nearest hatch that isnt waiting for inject to pop
            
    async def getUpgrades(self, hatches):
        if self.units(SPAWNINGPOOL).ready.noqueue.amount > 0:
            ups = await self.get_available_abilities(self.units(SPAWNINGPOOL).ready.noqueue[0])
            for up in ups:
                if self.can_afford(up):
                    await self.do(self.units(SPAWNINGPOOL).ready.noqueue[0](up))
                    break
        if self.units(ROACHWARREN).ready.amount > 0 and self.units(LAIR).ready.amount > 0 and self.can_afford(GLIALRECONSTITUTION):
            if RESEARCH_GLIALREGENERATION in await self.get_available_abilities(self.units(ROACHWARREN)[0]):
                await self.do(self.units(ROACHWARREN)[0](RESEARCH_GLIALREGENERATION))
            elif RESEARCH_TUNNELINGCLAWS in await self.get_available_abilities(self.units(ROACHWARREN)[0]) and self.units(ROACHWARREN)[0].noqueue:
                await self.do(self.units(ROACHWARREN)[0](RESEARCH_TUNNELINGCLAWS))
        if hatches.ready.amount > 0 and self.can_afford(BURROW):
            if RESEARCH_BURROW in await self.get_available_abilities(hatches[0]):
                await self.do(hatches[0](RESEARCH_BURROW))
        if self.units(EVOLUTIONCHAMBER).ready.noqueue.amount > 0 and (self.units(LAIR).amount > 0 or self.units(HIVE).amount > 0):
            for evo in self.units(EVOLUTIONCHAMBER).ready.noqueue:
                possibleUpgrades = await self.get_available_abilities(evo)
                for ups in possibleUpgrades:
                    if self.can_afford(ups):
                        await self.do(evo(ups))
                        break
        if self.units(HYDRALISKDEN).ready.noqueue.amount > 0:
            possibleUpgrades = await self.get_available_abilities(self.units(HYDRALISKDEN).ready.noqueue[0])
            for ups in possibleUpgrades:
                if self.can_afford(ups):
                    await self.do(self.units(HYDRALISKDEN).ready.noqueue[0](ups))
                    break
        if self.units(LURKERDENMP).ready.noqueue.amount > 0 and self.units(HIVE).ready.amount > 0:
            upgrades = await self.get_available_abilities(self.units(LURKERDENMP).ready.noqueue[0])
            for ups in upgrades:
                if self.can_afford(ups):
                    await self.do(self.units(LURKERDENMP).ready.noqueue[0](ups))

    #update to use attack/retreat flags
    def timeToAttack(self, hatches):
        ownedUnits = self.units
        if (ownedUnits(BROODLORD).ready.amount > 4 and self.supply_used > 195): # has broodlords and is ready to kill
            return True
        if self.enemyIsGoingForAir and self.supply_used > 185 and self.units(HYDRALISK).ready.amount > 30:
            return True
        if self.gameTime > self.SECONDS_PER_MINUTE * 30: #fail safe --> prevents getting stuck and drawing
            return True
        if self.supply_used > 185 and (self.minerals > 1000 and self.vespene > 500): #free up some army supply / end game if enemy is weak
            return True
        if self.goHard:
            return True
        self.defendCreep()
        if self.defendTheCreep:
            return True
        return False


    #methods that set if it is time to attack or not
    async def timeToGoHard(self):
        if not self.goHard and self.units(ZERGLING).ready.amount >= 14 and self.gameTime < self.SECONDS_PER_MINUTE * 4:
            self.goHard = True
            await self.chat_send("Going Hard")
        elif self.goHard and self.units(ZERGLING).amount <= 4:
            self.goHard = False
            await self.chat_send("Chilling")

    # if defence is very successful then counter attack --> implement this
    def defendCreep(self):
        enemys = self.known_enemy_units
        tumors = self.units(CREEPTUMOR) | self.units(CREEPTUMORBURROWED) | self.units(CREEPTUMORQUEEN)
        if tumors.exists and enemys.exists:
            for e in enemys:
                if e.distance_to(tumors.closest_to(e.position).position) < 9:
                    self.defendTheCreep = True
                    return
            self.defendTheCreep = False
            return



    #methods that set flags to change generic playstyle
    async def enemyGoingAir(self): # switch production over to straight hydras :)
        if not self.enemyIsGoingForAir:
            if self.known_enemy_units(VOIDRAY).amount > 1:
                self.enemyIsGoingForAir = True
                #await self.chat_send("VOID RAYS!")
                print ("enemy going for voids: MAKE HYDRAS!!!!")

    async def controlArmy(self, hatches): # need to write defense for target air units
        await self.timeToGoHard()
        allArmyUnits = self.units(ROACH).ready | self.units(HYDRALISK).ready | self.units(ZERGLING).ready | self.units(CORRUPTOR).ready | self.units(BROODLORD).ready | self.units(LURKERMP).ready
        targetGround = self.units(ZERGLING).ready | self.units(ROACH).ready | self.units(BROODLORD).ready # cant shoot up --> make for 4 catagories of units --> ground att only, air att only, both, spellcaster(no attack)
        targetGroundAndAir = self.units(HYDRALISK).ready
        targetAir = self.units(CORRUPTOR).ready
        attacking = self.timeToAttack(hatches)
        
        groundTargets = self.known_enemy_units.not_flying.not_structure.not_useless | self.known_enemy_structures(PHOTONCANNON) | self.known_enemy_structures(BUNKER) | self.known_enemy_structures(SHIELDBATTERY) | self.known_enemy_structures(MISSILETURRET) | self.known_enemy_structures(SPINECRAWLER) | self.known_enemy_units(SPORECRAWLER) | self.known_enemy_units(PLANETARYFORTRESS)
        airTargets = self.known_enemy_units.flying.not_structure.not_useless
        buildings = self.known_enemy_structures.not_useless
        groundAndAirTargets = groundTargets | airTargets
        
        if attacking:
            if self.known_enemy_units.amount > 0:
                for s in targetGround:
                    await self.do(s.attack(self.find_ground_target(s, groundTargets, buildings)))
                for s in targetGroundAndAir:
                    await self.do(s.attack(self.find_any_target(s, groundAndAirTargets, buildings)))
                for s in targetAir:
                    await self.do(s.attack(self.find_air_target(s, airTargets, buildings)))
                for s in self.units(LURKERMP).ready:
                    if groundTargets.amount > 0 and groundTargets.closest_to(s.position).distance_to(s.position) < 9:
                        await self.do(s.stop())
                    else:
                        await self.do(s.move(self.find_ground_target(s, groundTargets, buildings).position))
                
        else: # self.iteration % 6 == 0: # defend
            if len(groundTargets) > 0:
                for s in targetGround:
                    closeTo = groundTargets.prefer_close_to(s.position)[0]
                    if closeTo.distance_to(s.position) < 10:
                            await self.do(s.attack(closeTo))
                    else:
                        for h in hatches:
                            if groundTargets.prefer_close_to(h.position)[0].distance_to(h.position) < 20:
                                await self.do(s.attack(closeTo))
                for s in self.units(LURKERMP).ready:
                    closeTo = groundTargets.prefer_close_to(s.position)[0]
                    if closeTo.distance_to(s.position) < 9:
                        await self.do(s.stop())
                    else:
                        for h in hatches:
                            if groundTargets.prefer_close_to(h.position)[0].distance_to(h.position) < 20:
                                await self.do(s.move(closeTo))
                                
            if len(groundAndAirTargets) > 0:
                for s in targetGroundAndAir:
                    closeTo = groundAndAirTargets.prefer_close_to(s.position)[0]
                    if closeTo.distance_to(s.position) < 10:
                            await self.do(s.attack(closeTo))
                    else:
                        for h in hatches:
                            if groundAndAirTargets.prefer_close_to(h.position)[0].distance_to(h.position) < 20:
                                await self.do(s.attack(closeTo))
            
            if len(airTargets) > 0:
                for s in targetAir:
                    closeTo = airTargets.prefer_close_to(s.position)[0]
                    if closeTo.distance_to(s.position) < 10:
                            await self.do(s.attack(closeTo))
                    else:
                        for h in hatches:
                            if self.known_enemy_units.prefer_close_to(h.position)[0].distance_to(h.position) < 20:
                                await self.do(s.attack(closeTo))
            #moves army units not doing anything to the "forward most" hatch  closest to enemy (positioning code if no enemy to fight)
            for s in allArmyUnits.idle:
                closestBaseToMe = hatches.closest_to(s.position)
                await self.do(s.move(closestBaseToMe))
            
                            
        if self.units(QUEEN).amount > 0 and len(self.known_enemy_units) > 0:
            for s in hatches:
                if self.known_enemy_units.prefer_close_to(s.position)[0].distance_to(s.position) < 10:
                    for q in self.units(QUEEN):
                        await self.do(q.attack(self.known_enemy_units.prefer_close_to(q.position)[0]))
        if self.units(QUEEN).keep_tags(self.creepSpreadQueens).amount > 0: #creepSpreadQueen Logic
            tumors = self.units(CREEPTUMOR) | self.units(CREEPTUMORBURROWED) | self.units(CREEPTUMORQUEEN) | self.units(HATCHERY) | self.units(LAIR) | self.units(HIVE)
            for q in self.units(QUEEN).keep_tags(self.creepSpreadQueens).doesnt_have_orders([BUILD_CREEPTUMOR, ATTACK]):
                if q.distance_to(tumors.closest_to(q.position)) > 20:
                    closestBaseToMe = tumors.closest_to(q.position)
                    await self.do(q.move(closestBaseToMe))
        if self.units(QUEEN).remove_tags(self.creepSpreadQueens).amount > 0: #injectQueen logic
            for q in self.units(QUEEN).remove_tags(self.creepSpreadQueens).doesnt_have_orders([EFFECT_INJECTLARVA, ATTACK]):
                if q.is_idle:
                    closestBaseToMe = hatches.closest_to(q.position)
                    await self.do(q.move(closestBaseToMe))
                elif q.distance_to(hatches.closest_to(q.position)) > 20:
                    closestBaseToMe = hatches.closest_to(q.position)
                    await self.do(q.move(closestBaseToMe))
                        
        if self.units(OVERSEER).amount > 0 and allArmyUnits.amount > 0: # keeps overseer with AllArmyUnits
            for o in self.units(OVERSEER).ready.idle:
                moveHere = allArmyUnits.closest_to(o.position)
                if not moveHere.position == o.position:
                    await self.do(o.move(moveHere))
                                  
    async def transfuse(self):
        queens = self.units(QUEEN)
        for queen in queens:
            if TRANSFUSION_TRANSFUSION in await self.get_available_abilities(queen):
                for unit in self.units.not_structure.owned:
                    if not unit.is_structure and unit.health_max - 125 >= unit.health and unit.distance_to(queen.position) < 15 and not unit.tag == queen.tag: #range of transfuse is 7
                        await self.do(queen(TRANSFUSION_TRANSFUSION, unit))
            
    async def roachMicro(self):
        roaches = self.units(ROACH)
        roachburrowed = self.units(ROACHBURROWED)
        for roach in roaches:
            if BURROWDOWN_ROACH in await self.get_available_abilities(roach) and roach.health < 60 and self.can_afford(BURROWDOWN_ROACH):
                await self.do(roach(BURROWDOWN_ROACH))
        for roach in roachburrowed:
            if BURROWUP_ROACH in await self.get_available_abilities(roach) and roach.health > 120 and self.can_afford(BURROWUP_ROACH):
                    await self.do(roach(BURROWUP_ROACH))

    #lurker range is 9
    async def lurkerMicro(self):
        enemyUnits = self.known_enemy_units.not_flying.not_useless
        lurkers = self.units(LURKERMP).ready
        lurkersBurrowed = self.units(LURKERMPBURROWED)
        if enemyUnits.amount > 0:
            for l in lurkers:
                if BURROWDOWN_LURKER in await self.get_available_abilities(l) and enemyUnits.closest_to(l.position).distance_to(l.position) < 9:
                    await self.do(l(BURROWDOWN_LURKER))
            for l in lurkersBurrowed:
                if BURROWUP_LURKER in await self.get_available_abilities(l) and enemyUnits.closest_to(l.position).distance_to(l.position) > 9:
                    await self.do(l(BURROWUP_LURKER))
        else:
            for l in lurkersBurrowed:
                await self.do(l(BURROWUP_LURKER))


    #get list of all expansions --> mark them off as they are scouted:
    #once all are reached --> reset list and go again until enemy is found:

#   async def find_enemy(self, unit):



    #TODO make units target fire / finish hurt units
    def find_ground_target(self, unit, enemyUnits, enemyBuildings):
        if len(enemyUnits) > 0:
            return enemyUnits.prefer_close_to(unit.position)[0]
        elif len(enemyBuildings) > 0:
            return enemyBuildings.prefer_close_to(unit.position)[0]
        else:
            if(len(self.enemy_start_locations) > 1):
                return random.sample(self.enemy_start_locations, 1)[0]
            return self.enemy_start_locations[0]
        
    def find_any_target(self, unit, enemyUnits, enemyBuildings):
        if len(enemyUnits) > 0:
            return enemyUnits.prefer_close_to(unit.position)[0]
        elif len(enemyBuildings) > 0:
            return enemyBuildings.prefer_close_to(unit.position)[0]
        else:
            if(len(self.enemy_start_locations) > 1):
                return random.sample(self.enemy_start_locations, 1)[0]
            return self.enemy_start_locations[0]
        
    def find_air_target(self, unit, enemyUnits, enemyBuildings):
        if len(enemyUnits) > 0:
            return enemyUnits.prefer_close_to(unit.position)[0]
        elif len(enemyBuildings) > 0:
            return enemyBuildings.prefer_close_to(unit.position)[0]
        else:
            if(len(self.enemy_start_locations) > 1):
                return random.sample(self.enemy_start_locations, 1)[0]
            return self.enemy_start_locations[0]
        
    async def morphOverseer(self):
        if self.units(OVERSEER).amount < 2 and self.can_afford(OVERSEER) and self.units(OVERLORD).amount >= 2 and (self.units(LAIR).ready.amount > 0 or self.units(HIVE).amount > 0) and self.units(OVERLORDCOCOON).amount < 2:
            await self.do(self.units(OVERLORD).ready.noqueue[0](MORPH_OVERSEER))
           
    async def morphBroodlord(self):
        if self.can_afford(BROODLORD) and self.units(GREATERSPIRE).ready.amount > 0 and self.supply_left > 1:
            for c in self.units(CORRUPTOR).ready:
                if self.can_afford(BROODLORD):
                    await self.do(c(MORPHTOBROODLORD_BROODLORD))
                    
    async def morphLurker(self):
        if self.can_afford(LURKERMP) and self.units(LURKERDENMP).ready.amount > 0 and self.supply_left > 0 and self.units(HYDRALISK).ready.amount / 4 > (self.units(LURKERMP).amount + self.units(LURKERMPEGG).amount):
            for h in self.units(HYDRALISK).ready:
                if self.can_afford(LURKERMP):
                    await self.do(h(MORPH_LURKER))
    
        
#run_game(maps.get("CatalystLE"), [Bot(Race.Zerg, SnowBot()), Computer(Race.Terran, Difficulty.Harder)], realtime = False)